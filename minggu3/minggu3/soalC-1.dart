void main() {
  for (var x = 1; x <= 20; x++) {
    x % 2 == 1 && x % 3 == 0
        ? print("${x} - I Love Coding")
        : x % 2 == 0
            ? print("${x} - Berkualitas")
            : print("${x} - Santai");
  }
}
