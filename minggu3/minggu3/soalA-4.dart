import 'dart:io';
main() {
  stdout.write("Nilai x: ");
  double x = double.parse(stdin.readLineSync()!);
  stdout.write("Nilai y: ");
  double y = double.parse(stdin.readLineSync()!);

  double jumlah;

  //penjumlahan
  jumlah = x + y;
  print("$x + $y = $jumlah");
  //pengurangan
  jumlah = x - y;
  print("$x - $y = $jumlah");
  //perkalian
  jumlah = x * y;
  print("$x * $y = $jumlah");
  //pembagian
  jumlah = x / y;
  print("$x / $y = $jumlah");
}