void main() {
  var firstWord = 'I';
  var secondWord = 'am';
  var thirdWord = 'going';
  var fourthWord = 'to';
  var fifthWord = 'be';
  var sixthWord = 'Flutter';
  var seventhWord = 'Developer';

  print('First Word: ' + firstWord);
  print('Second Word: ' + secondWord);
  print('Third Word: ' + thirdWord);
  print('Fourth Word: ' + fourthWord);
  print('Fifth Word: ' + fifthWord);
  print('Sixth Word: ' + sixthWord);
  print('Seventh Word: ' + seventhWord);
  
}