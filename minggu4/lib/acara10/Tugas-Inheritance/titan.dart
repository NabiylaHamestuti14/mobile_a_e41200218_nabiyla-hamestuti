class Titan {
  //powerPoint
  int _powerPoint = 0;

  //get powerPoint
  int get powerPoint => _powerPoint;
  //set powerPoint
  set powerPoint(int value) {
    _powerPoint = value;
  }
}
