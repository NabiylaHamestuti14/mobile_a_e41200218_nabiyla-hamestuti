import 'bangun_datar.dart';

//BangunDatar merupakan parent class
//Segitiga merupakan child class
class Segitiga extends BangunDatar {
  double alas = 0;
  double tinggi = 0;
  double diagonal = 0;

  @override
  double luas() {
    //rumus luas segitiga
    return 0.5 * alas * tinggi;
  }

  @override
  double keliling() {
    //rumus keliling segitiga
    return alas + tinggi + diagonal;
  }
}
