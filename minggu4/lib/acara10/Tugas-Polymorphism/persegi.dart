import 'bangun_datar.dart';

//BangunDatar merupakan parent class
//Persegi merupakan child class
class Persegi extends BangunDatar {
  double sisi = 0;

  @override
  double luas() {
    //rumus luas persegi
    return sisi * sisi;
  }

  @override
  double keliling() {
    //rumus keliling persegi
    return 4 * sisi;
  }
}
