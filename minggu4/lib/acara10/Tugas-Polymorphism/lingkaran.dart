import 'bangun_datar.dart';

//BangunDatar merupakan parent class
//Lingkaran merupakan chiild class
class Lingkaran extends BangunDatar {
  double radius = 0;

  @override
  double luas() {
    //rumus luas lingkaran
    return 3.14 * radius * radius;
  }

  @override
  double keliling() {
    //rumus keliling lingkaran
    return 2 * 3.14 * radius;
  }
}
