class ChartModel {
  final String name;
  final String message;
  final String time;
  final String profileUrl;

  ChartModel(
      {required this.name,
      required this.message,
      required this.time,
      required this.profileUrl});
}

final List<ChartModel> items = [
  ChartModel(
      name: 'jk',
      message: 'hallo jim',
      time: '12.00',
      profileUrl:
          'assets/images/jk.jpg'),
  ChartModel(
      name: 'suga',
      message: 'hi..',
      time: '11.00',
      profileUrl:
          'assets/images/suga.jpg'),
  ChartModel(
      name: 'hobi',
      message: 'hello ',
      time: '10.00',
      profileUrl:
          'assets/images/hobi.jpg'),
  ChartModel(
      name: 'namjoon',
      message: 'hello hello',
      time: '09.50',
      profileUrl:
          'assets/images/namjoon.jpg'),
  ChartModel(
      name: 'v',
      message: 'hi..',
      time: '09.30',
      profileUrl:
          'assets/images/v.jpg'),
  ChartModel(
      name: 'jin',
      message: 'hallo',
      time: '08.00',
      profileUrl:
          'assets/images/jin.jpg'),
  ];
