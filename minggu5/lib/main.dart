import 'package:flutter/material.dart';
import 'tugas/Telegram.dart';
import 'belajarflutter.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
         '/': (context) => Telegram(),
        //'/': (context) => belajarflutter(),
      },
    );
  }
}
